using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foxay.DotnetSerializer.Models
{
    public class Base64Settings
    {
        public SerializationFormat CleanTextFormat { get; set; }
        public Encoding? Encoding { get; set; }
    }
}