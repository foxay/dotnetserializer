using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Foxay.DotnetSerializer.Models
{
    public class XmlSettings
    {
        public Dictionary<string,string>? Namespaces { get; set; }
        public XmlReaderSettings? ReaderSettings { get; set; }
        public XmlWriterSettings? WriterSettings { get; set; }
    }
}