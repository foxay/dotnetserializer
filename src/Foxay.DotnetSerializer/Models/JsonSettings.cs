using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foxay.DotnetSerializer.Models
{
    public class JsonSettings
    {
        public JsonSerializerSettings? Settings { get; set; }
    }
}