using System.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foxay.DotnetSerializer
{
    public class Serializer
    {
        public static Serializer WithSettings(Action<SerializerSettings> settingsSet)
        {
            SerializerSettings settings = new();
            settingsSet?.Invoke(settings);
            Serializer serializer = new(settings);
            return serializer;
        }

        private SerializerSettings _settings;

        private Serializer(SerializerSettings settings)
        {
            _settings = settings;
        }

        public string? Serialize(
            object? payloadData, SerializationFormat format)
        {
            SerializerServiceBase? service = Resolver.ResolveSerializationService(
                format,
                jsonSettings: new JsonSettings
                {
                    Settings = new JsonSerializerSettings
                    {

                    }
                },
                xmlSettings: new XmlSettings
                {
                    ReaderSettings = new XmlReaderSettings
                    {
                        
                    },
                    WriterSettings = new XmlWriterSettings
                    {
                        Indent = _settings.Indent,
                        OmitXmlDeclaration = _settings.OmitXmlDeclaration
                    },
                    Namespaces = _settings.Namespaces
                },
                base64Settings: new Base64Settings
                {
                    Encoding = _settings.Base64Encoding,
                    CleanTextFormat = _settings.Base64CleanTextFormat
                });
            if(service is null)
            {
                return null;
            }
            string? data = service.Serialize(payloadData);
            return data;
        }

        public DocumentObjectModel SerializeToDOM(
            object? payloadData, SerializationFormat format)
        {
            SerializerServiceBase? service = Resolver.ResolveSerializationService(
                format,
                jsonSettings: new JsonSettings
                {
                    Settings = new JsonSerializerSettings
                    {

                    }
                },
                xmlSettings: new XmlSettings
                {
                    ReaderSettings = new XmlReaderSettings
                    {
                        
                    },
                    WriterSettings = new XmlWriterSettings
                    {
                        Indent = _settings.Indent,
                        OmitXmlDeclaration = _settings.OmitXmlDeclaration
                    }
                },
                base64Settings: new Base64Settings
                {
                    Encoding = _settings.Base64Encoding,
                    CleanTextFormat = _settings.Base64CleanTextFormat
                }
            );
            if(service is null)
            {
                return new DocumentObjectModel();
            }
            string? data = service.Serialize(payloadData);
            if(data is null)
            {
                return new DocumentObjectModel();
            }

            DocumentObjectModel dom = new();
            return dom;
        }

        public TResultType? Deserialize<TResultType>(
            DocumentObjectModel dom, SerializationFormat format) where TResultType : class
        {
            string? data = dom.ToString();
            return Deserialize<TResultType>(data, format);
        }

        public TResult? Deserialize<TResult>(
            string? data, SerializationFormat format) where TResult : class
        {
            return (TResult?)Deserialize(typeof(TResult), data, format);
        }

        public object? Deserialize(
            Type targetType, string? data, SerializationFormat format)
        {
            SerializerServiceBase? service = Resolver.ResolveSerializationService(
                format,
                jsonSettings: new JsonSettings
                {
                    Settings = new JsonSerializerSettings
                    {

                    }
                },
                xmlSettings: new XmlSettings
                {
                    ReaderSettings = new XmlReaderSettings
                    {
                        
                    },
                    WriterSettings = new XmlWriterSettings
                    {
                        Indent = _settings.Indent,
                        OmitXmlDeclaration = _settings.OmitXmlDeclaration
                    }
                },
                base64Settings: new Base64Settings
                {
                    Encoding = _settings.Base64Encoding,
                    CleanTextFormat = _settings.Base64CleanTextFormat
                }
            );
            if(service is null)
            {
                return null;
            }
            object? objectData = service.Deserialize(targetType, data);
            return objectData;
        }

        public string? ConvertBetweenFormats<TDataModel>(
            string? data, 
            SerializationFormat formatConvertFrom, 
            SerializationFormat formatConvertTo) where TDataModel : class
        {
            if(data is null)
            {
                return null;
            }
            TDataModel temporaryModel = Deserialize<TDataModel>(data, formatConvertFrom);
            string convertedData = Serialize(temporaryModel, formatConvertTo);
            return convertedData;
        }

        public string? ConvertJsonToXml(string? json, string xmlRoot)
        {
            if(json is null)
            {
                return null;
            }
            XmlDocument? xmlDoc = JsonConvert.DeserializeXmlNode(json, xmlRoot);
            if(xmlDoc is null)
            {
                return null;
            }
            using var stringWriter = new StringWriter();
            using var xmlTextWriter = XmlWriter.Create(stringWriter);
            xmlDoc.WriteTo(xmlTextWriter);
            xmlTextWriter.Flush();
            return stringWriter.GetStringBuilder().ToString();
        }
    }
}