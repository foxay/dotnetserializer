using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Foxay.DotnetSerializer.Serializers
{
    internal class XmlSerializer : SerializerServiceBase
    {
        private readonly XmlReaderSettings? _readerSettings;
        private readonly XmlWriterSettings? _writerSettings;
        private readonly XmlSerializerNamespaces? _namespaces;

        public XmlSerializer()
        {
        }
        public XmlSerializer(
            XmlReaderSettings readerSettings, 
            XmlWriterSettings writerSettings, 
            Dictionary<string, string> namespaces)
        {
            _readerSettings = readerSettings;
            _writerSettings = writerSettings;
            if(namespaces is not null)
            {
                _namespaces = new XmlSerializerNamespaces();
                _namespaces.Add("", ""); // "hack" to omnit default xsi and xsd namespaces
                foreach((string prefix, string value) in namespaces)
                {
                    _namespaces.Add(prefix, value);
                }
            }
        }

        public override object? Deserialize(Type targetType, string? data)
        {
            if (data is null)
            {
                return default;
            }

            System.Xml.Serialization.XmlSerializer serializer = new(targetType, defaultNamespace: string.Empty);
            using StringReader reader = new(data);
			XmlReader xmlReader = XmlReader.Create(reader, _readerSettings);
            object? obj = serializer.Deserialize(xmlReader);
            return obj;
        }

        public override string? Serialize(object? objectData)
        {
			if (objectData is null)
            {
                return null;
            }

            System.Xml.Serialization.XmlSerializer serializer = new(objectData.GetType(), defaultNamespace: "");
            using StringWriter stream = new();
            using XmlWriter writer = XmlWriter.Create(stream, _writerSettings);
            serializer.Serialize(writer, objectData, _namespaces);
            return stream.ToString();
        }

        // public override DocumentObjectModel ToDOM(string? data)
        // {
        //     using StringReader reader = new(data);
		// 	XmlReader xmlReader = XmlReader.Create(reader, _readerSettings);
        //     DocumentObjectModel dom = new();
        //     while(xmlReader.HasValue)
        //     {
        //         dom.Attributes = new();
        //         for(int i = 0; i < xmlReader.AttributeCount; i++)
        //         {
        //             xmlReader.GetAttribute(i);
        //             //dom.Attributes.Add()
        //         }
        //     }
        //     return dom;
        // }
    }
}