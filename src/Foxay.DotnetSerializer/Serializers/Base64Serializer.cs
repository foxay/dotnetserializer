using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Foxay.DotnetSerializer.Serializers
{
    internal class Base64Serializer : SerializerServiceBase
    {
        private readonly SerializerServiceBase? _helperSerializer;
        private readonly Encoding _encoding;

        public Base64Serializer(
            SerializationFormat cleanTextFormat,
            Encoding encoding,
            JsonSettings? jsonSettings = null,
            XmlSettings? xmlSettings = null )
        {
            _encoding = encoding;
            _helperSerializer = Resolver.ResolveSerializationService(
                cleanTextFormat switch
                {
                    SerializationFormat.JSON => SerializationFormat.JSON,
                    SerializationFormat.XML => SerializationFormat.XML,
                    _ => SerializationFormat.JSON
                },
                jsonSettings,
                xmlSettings
            );
        }

        private Encoding Encoding => _encoding is null ? Encoding.UTF8 : _encoding;

        // from encoded base64 to formated string, then to object
        public override object? Deserialize(Type targetType, string? data)
        {
            if(data is null)
            {
                return default;
            }
            byte[] bytes = Convert.FromBase64String(data);
            string decodedText = Encoding.GetString(bytes);
            if(targetType == typeof(String) || _helperSerializer is null)
            {
                return decodedText;
            }
            object? decodedObject = _helperSerializer.Deserialize(targetType, decodedText);
            return decodedObject;
        }

        // from object to serialized string, then to encoded base64
        public override string? Serialize(object? objectData)
        {
            if(_helperSerializer is null)
            {
                return default;
            }
            
            string? temporaryFormatedText = _helperSerializer.Serialize(objectData);
            if(temporaryFormatedText is null)
            {
                return default;
            }

            byte[] bytes = Encoding.GetBytes(temporaryFormatedText);
            string encodedText = Convert.ToBase64String(bytes);
            return encodedText;
        }
    }
}