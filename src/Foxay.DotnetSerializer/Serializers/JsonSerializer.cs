using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foxay.DotnetSerializer.Serializers
{
    internal class JsonSerializer : SerializerServiceBase
    {
        private readonly JsonSerializerSettings _settings;

        public JsonSerializer(JsonSerializerSettings settings)
        {
            _settings = settings;
        }

        public override object? Deserialize(Type targetType, string? data)
        {
            if(data is null)
            {
                return default;
            }
            object? objectData = JsonConvert.DeserializeObject(data, targetType, _settings);
            return objectData;
        }

        public override string? Serialize(object? objectData)
        {
            if(objectData is null)
            {
                return default;
            }
            string? data = JsonConvert.SerializeObject(objectData, _settings);
            return data;
        }
    }
}