using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foxay.DotnetSerializer.Serializers
{
    internal class Resolver
    {
        public static SerializerServiceBase? ResolveSerializationService(
            SerializationFormat format, 
            JsonSettings? jsonSettings = null,
            XmlSettings? xmlSettings = null,
            Base64Settings? base64Settings = null) => 
            format switch
            {
                SerializationFormat.JSON => new DotnetSerializer.Serializers.JsonSerializer(
                    jsonSettings?.Settings),
                SerializationFormat.XML => new DotnetSerializer.Serializers.XmlSerializer(
                    xmlSettings?.ReaderSettings,
                    xmlSettings?.WriterSettings,
                    xmlSettings?.Namespaces),
                SerializationFormat.Base64 => new Base64Serializer(
                    base64Settings.CleanTextFormat,
                    base64Settings.Encoding,
                    jsonSettings,
                    xmlSettings),
                _ => default
            };

    }
}