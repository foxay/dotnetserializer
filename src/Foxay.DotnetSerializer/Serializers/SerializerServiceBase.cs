using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foxay.DotnetSerializer.Serializers
{
    internal abstract class SerializerServiceBase
    {
        public abstract string? Serialize(object? objectData);
        public abstract object? Deserialize(Type targetType, string? data);
    }
}