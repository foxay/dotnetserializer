using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foxay.DotnetSerializer
{
    public class DocumentObjectModel : Dictionary<string, DocumentObjectModel>
    {
        public Dictionary<string, string>? Attributes { get; set; }
        public string? Value { get; set; }
    }
}