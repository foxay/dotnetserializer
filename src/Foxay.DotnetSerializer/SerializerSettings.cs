using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Foxay.DotnetSerializer
{
    public class SerializerSettings
    {
        public Dictionary<string,string> Namespaces { get; set; }
        public bool OmitXmlDeclaration { get; set; }
        public bool Indent { get; set; }
        public SerializationFormat Base64CleanTextFormat {get; set;}
        public Encoding? Base64Encoding { get; set; }

    }
}