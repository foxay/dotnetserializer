using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tests.Data
{
    public class SerializerTestsDataStruct
    {
        public SerializerTestsDataStruct(SerializationFormat format, string? textData)
        {
            Format = format;
            TextData = textData;
        }

        public SerializationFormat Format { get; set; }
        public string? TextData { get; set; }
    }

    public class SerializerTestsData : IEnumerable<object[]>
    {
        public IEnumerator<object[]> GetEnumerator()
        {
            List<object[]> testCases = new()
            {
                new object[]{ SerializationFormat.XML, null},
                new object[]{ SerializationFormat.JSON, null},
                new object[]{ SerializationFormat.Base64, null},

                new object[]{ SerializationFormat.XML, "<Model><Id>1</Id><Name>qwe</Name><Timestamp>2000-05-15T05:55:45.125</Timestamp></Model>"},

                new object[]{ SerializationFormat.JSON, "{\"Id\":1,\"Name\":\"qwe\",\"Timestamp\":\"2000-05-15T05:55:45.125\"}"},

                new object[]{ SerializationFormat.Base64, "eyJJZCI6MSwiTmFtZSI6InF3ZSIsIlRpbWVzdGFtcCI6IjIwMDAtMDUtMTVUMDU6NTU6NDUuMTI1In0="},
            };

            return testCases.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    }
}