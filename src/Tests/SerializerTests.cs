using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;
using SerializationFormat = Foxay.DotnetSerializer.SerializationFormat;

namespace Tests
{
    public class SerializerTests
    {

        [Theory]
        [InlineData(SerializationFormat.XML, "Resources/XmlModel.xml")]
        public void DeserializeFromFileTest(SerializationFormat format, string file)
        {
            // Arrange
            ModelMock? expectedModel = new()
            {
                Id = 1,
                Name = "qwe",
                // year, month, day, hour, minutes, seconds, milliseconds
                Timestamp = new DateTime(2000, 05, 15, 05, 55, 45, 125) 
            };

            // Act
            string xmlFile = File.ReadAllText(file);            
            ModelMock? actualModel = Serializer
                .WithSettings((settings)=>
                {
                    settings.OmitXmlDeclaration = true;
                    settings.Namespaces = new Dictionary<string, string>();
                })
                .Deserialize<ModelMock>(xmlFile, format);

            // Assert
            Assert.Equal(expectedModel, actualModel);
        }

        [Theory]
        [ClassData(typeof(SerializerTestsData))]
        public void DeserializeTest(SerializationFormat format, string? textData)
        {
            // Arrange
            ModelMock? expectedModel = textData is null ? null : new()
            {
                Id = 1,
                Name = "qwe",
                // year, month, day, hour, minutes, seconds, milliseconds
                Timestamp = new DateTime(2000, 05, 15, 05, 55, 45, 125) 
            };

            // Act
            ModelMock? actualModel = Serializer
                .WithSettings((settings)=>
                {
                    settings.OmitXmlDeclaration = true;
                    settings.Namespaces = new Dictionary<string, string>();
                    settings.Base64CleanTextFormat = SerializationFormat.JSON;
                    settings.Base64Encoding = Encoding.UTF8;
                })
                .Deserialize<ModelMock>(textData, format);

            // Assert
            Assert.Equal(expectedModel, actualModel);
        }

        [Theory]
        [ClassData(typeof(SerializerTestsData))]
        public void SerializeTest(SerializationFormat format, string? expectedData)
        {
            // Arrange
            ModelMock? model = expectedData is null ? null : new()
            {
                Id = 1,
                Name = "qwe",
                // year, month, day, hour, minutes, seconds, milliseconds
                Timestamp = new DateTime(2000, 05, 15, 05, 55, 45, 125) 
            };

            // Act
            string? actualData = Serializer
                .WithSettings((settings)=>
                {
                    settings.OmitXmlDeclaration = true;
                    settings.Namespaces = new Dictionary<string, string>();
                    settings.Base64CleanTextFormat = SerializationFormat.JSON;
                    settings.Base64Encoding = Encoding.UTF8;
                })
                .Serialize(model, format);

            // Assert
            Assert.Equal(expectedData, actualData);
        }
    }
}