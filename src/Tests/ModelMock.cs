using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Tests
{
    [XmlRoot("Model", Namespace = "")]
    public class ModelMock
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Timestamp { get; set; }

        public override bool Equals(object? obj)
        {
            if(obj is null)
            {
                return false;
            }
            ModelMock? instanceToCompare = obj as ModelMock;
            return instanceToCompare is ModelMock
                && instanceToCompare.Id == Id
                && instanceToCompare.Name == Name
                && instanceToCompare.Timestamp == Timestamp;
        }
    }
}